package com.maocq;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.maocq.model.Address;
import com.maocq.model.Tweet;
import com.maocq.model.User;

public class Main {

	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Persistencia");

	public static void main(String[] args) {
		/**
		 * Gestor de persistencia (EM)
		 */
		// EntityManagerFactory emf =
		// Persistence.createEntityManagerFactory("Persistencia");

		// EntityManager manager = emf.createEntityManager();
		// manager.getTransaction().begin();
		// User mao = new User(1L, "Mauricio", "carmonaesc@gmail.com");
		// manager.persist(mao);
		// manager.getTransaction().commit();
		// manager.close();

		insertUsers();
		updateUser();
		deleteUser();
		viewUsers();

		insertTweets();
		viewTweetsForUser();

		nativeQuery();
	}

	private static void insertUsers() {
		EntityManager manager = emf.createEntityManager();

		User mao = new User("Mauricio", "carmonaesc@gmail.com");
		mao.setAddress(new Address("Calle Falsa 123", "Buenos aires"));
		User susa = new User("Susana", "susa@gmail.com");
		User moni = new User("Monica", "moni@gmail.com");

		manager.getTransaction().begin();
		manager.persist(mao);
		manager.persist(susa);
		manager.persist(moni);

		susa.setName("Susanita");
		manager.getTransaction().commit();

		manager.close();
	}

	private static void updateUser() {
		EntityManager manager = emf.createEntityManager();

		manager.getTransaction().begin();
		User user = manager.find(User.class, 1L);
		user.setName("John Mauricio");
		user.setEmail("maocq@gmail.com");
		user.getAddress().setLocation("Springfield");
		manager.getTransaction().commit();

		manager.close();
	}

	private static void deleteUser() {
		EntityManager manager = emf.createEntityManager();

		manager.getTransaction().begin();
		// User user = manager.find(User.class, 3L);
		User user = (User) manager.createQuery("SELECT U FROM User U ORDER BY U.id DESC").setMaxResults(1)
				.getSingleResult();
		manager.remove(user);
		manager.getTransaction().commit();

		manager.close();
	}

	@SuppressWarnings("unchecked")
	private static void viewUsers() {
		EntityManager manager = emf.createEntityManager();

		List<User> usuarios = (List<User>) manager.createQuery("FROM User").getResultList();
		System.out.println("Numero de usuarios: " + usuarios.size());

		for (User user : usuarios) {
			System.out.println(user.toString());
		}

		manager.close();
	}

	private static void insertTweets() {
		EntityManager manager = emf.createEntityManager();
		manager.getTransaction().begin();

		User mauricio = manager.find(User.class, 1L);
		Tweet tweetM = new Tweet("Hello Tweet", mauricio);
		Tweet tweetM2 = new Tweet("Hello Tweet # 2", mauricio);
		manager.persist(tweetM);
		manager.persist(tweetM2);

		User valentina = new User("Valentina", "vale@gmail.com");
		manager.persist(valentina);
		manager.persist(new Tweet("Vale Tweet", valentina));

		manager.getTransaction().commit();
		manager.close();
	}

	private static void viewTweetsForUser() {
		EntityManager manager = emf.createEntityManager();

		User mauricio = manager.find(User.class, 1L);
		System.out.println(mauricio);
		List<Tweet> tweetsM = mauricio.getTwets();
		// Hack Lazy
		tweetsM.size();
		manager.close();

		for (Tweet tweet : tweetsM) {
			System.out.println(tweet);
		}
	}

	@SuppressWarnings("unchecked")
	private static void nativeQuery() {
		EntityManager manager = emf.createEntityManager();

		System.out.println(" SQL QUERY");
		String sql = "SELECT tw.* from tweets tw";

		List<Tweet> tweets = manager.createNativeQuery(sql, Tweet.class).getResultList();
		for (Tweet tweet : tweets) {
			System.out.println(tweet);
		}

		manager.close();
	}

}
